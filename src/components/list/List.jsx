import Card from "../card/Card";
import PropTypes from "prop-types";
import styles from "./List.module.css"
import {useDispatch, useSelector} from "react-redux";
import {changeModal, setFavorite} from "../../redux/actions/action";
import Button from "../Button/Button";

function List({data, cardClick}) {
    const favorite = useSelector(store => store.favorite)
    const dispatch = useDispatch()
    const buttonValues = cardClick ? {text: "Delete product (X)", type: "delete"} : {text: "Add to cart", type: "add"}
    return <ul className={styles.wrapper}>{data.map((product) => <Card key={product.id} product={product}
                                                                       cardClick={cardClick}
                                                                       isFavorite={favorite.includes(product.id)}
                                                                       handleFavorite={() => dispatch(setFavorite(product.id))}
                                                                       actionBtn={<Button text={buttonValues.text}
                                                                                          click={() => dispatch(changeModal(product.id, buttonValues.type))}/>}/>)}</ul>

}

List.propTypes = {
    data: PropTypes.array,
    handleClick: PropTypes.func,
    handleFavorite: PropTypes.func,
    favorite: PropTypes.array
}
export default List;
