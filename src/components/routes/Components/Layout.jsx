import Header from "../../Header/Header";
import {Outlet} from "react-router-dom";
import {useEffect, useState} from "react";
import Modal from "../../Modal/Modal";
import Button from "../../Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {addToBasket, changeModal, getProducts, removeItemBasket, setFavorite} from "../../../redux/actions/action";

function Layout() {
    const favorite = useSelector(store=>store.favorite)
    const basket = useSelector(store=>store.basket)
    const modal = useSelector(store=>store.modal)
const dispatch=useDispatch()

    useEffect(() => {
       dispatch(getProducts())
    }, [])

    return (
        <div>
            <Header favLength={favorite.length} cardLength={basket.length}/>
            <Outlet/>
            {modal.type === "add" &&
                <Modal click={()=>dispatch(changeModal())} header={"Modal add"} closeButton={true}
                       text={"Add product to card?"}
                       actions={<div className={"button-wrapper"}><Button text={"Ok"} bgColor={"green"}
                                                                          click={() => {
                                                                              dispatch(addToBasket(modal.id))
                                                                              dispatch(changeModal())
                                                                          }}/><Button text={"Cancel"}
                                                                                      bgColor={"green"}
                                                                                      click={()=>dispatch(changeModal())}/>
                       </div>}/>}
            {modal.type === "delete" &&
                <Modal click={()=>dispatch(changeModal())} header={"Delete modal?"} closeButton={true}
                       text={"Delete this product?"}
                       actions={<div className={"button-wrapper"}><Button text={"Ok"} bgColor={"green"}
                                                                          click={() => {

                                                                             dispatch(removeItemBasket(modal.id))
                                                                              dispatch(changeModal())
                                                                          }}/><Button text={"Cancel"}
                                                                                      bgColor={"green"}
                                                                                      click={()=>dispatch(changeModal())}/>
                       </div>}/>}
        </div>
    );
}

export default Layout;