import List from "../../list/List";
import {useSelector} from "react-redux";
import EmptyListChecker from "../Components/EmptyListChecker";

function Home() {

    const products = useSelector(store => {

        return store.products
    })
    return (
        <div>
            <EmptyListChecker checkArr={products} message={"Empty favorite list!"}>

                <List data={products}/>
            </EmptyListChecker>
        </div>
    );
}

export default Home;