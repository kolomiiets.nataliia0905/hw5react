import {ReactComponent as StarIcon} from "../../img/star-fill.svg"
import PropTypes from "prop-types";
import styles from "./Card.module.css"

function Card({product, handleFavorite, isFavorite, actionBtn}) {
    const {name, price, image, article, id} = product
    const starColor = isFavorite ? "red" : "black"
    return <li className={styles.card}><StarIcon className={styles.star} style={{fill: starColor}}
                                                 onClick={handleFavorite}/><img className={styles.img}
                                                                                src={image}
                                                                                alt={name}/>
        <div><h3>{name}</h3><h4>{article}</h4><h4>{price}</h4>{actionBtn}
        </div>
    </li>
}

Card.propTypes = {
    actionBtn: PropTypes.element,
    product: PropTypes.object,
    handleFavorite: PropTypes.func,
    isFavorite: PropTypes.bool
}

export default Card;
