export const GET_PRODUCT="GET_PRODUCT"
export const SET_FAVOURITE='SET_FAVOURITE'
export const ADD_TO_BASKET="ADD_TO_BASKET"
export const REMOVE_ITEM_BASKET="REMOVE_ITEM_BASKET"
export const CHANGE_MODAL="CHANGE_MODAL"
export const CLEAR_BASKET="CLEAR_BASKET"
export const getProducts=()=> {
    return (dispatch) => {
        return fetch("./product.json").then((res) => res.json())
            .then((data) => {
                dispatch({type:GET_PRODUCT,payload:data})
            })
    }
}
export const setFavorite=(id)=>{
    return {type:SET_FAVOURITE,payload:id}
}
export const addToBasket=(id)=>{

    return {type:ADD_TO_BASKET,payload:id}

}
export const clearBasket=()=> {
    return {type:CLEAR_BASKET}
}
export const removeItemBasket=(id)=> {
    return {type:REMOVE_ITEM_BASKET,payload:id}
}
export const changeModal = (id, type) => {
    return {type:CHANGE_MODAL,payload:{type, id}}

}